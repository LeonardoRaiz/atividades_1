//Faça um programa que receba o valor de um depósito e o valor da taxa de juros, calcule e mostre o valor do rendimento e o valor total depois do rendimento

namespace exercicio8 {
    let dep, taxa, rend, total: number;

    dep = 1000;
    taxa = 20;

    rend = dep * taxa/100;
    total = dep + rend;

    console.log(`O rendimento final é de: ${rend}`);

    console.log(`O valor total do rendimento: ${total}`);
    
    
}