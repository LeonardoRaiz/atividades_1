//Faça um programa que calcule e mostre a área de um triângulo. Sabe-se que: Área = (base * altura)/2

namespace exercicio9 {
    let base, altura, area: number;

    base = 20;
    altura = 40;

    area = (base * altura)/2

    console.log(`Área do triângulo: ${area}`);
    
}