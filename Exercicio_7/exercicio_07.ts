//Faça um programa que receba o salário-base de um funcionário, calcule e mostre o seu salário a receber, sabendo-se que esse funcionário tem gratificação de R$50.00 e paga 10% sobre o salário-base

namespace exercicio7 {
  let salario, salreceber, imp: number;

  salario = 1000;
  imp = (salario * 10) / 100;
  salreceber = salario + 50 - imp;

  console.log(`O novo salário do funcionário é de: ${salreceber}`);
}
