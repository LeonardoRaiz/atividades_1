//Faça um programa que receba o salário-base de um funcionário, calcule e mostre o salário a receber, sabendo-se que esse funcionário tem gratificação de 5% sobre o salário-base e pagaimposto de 7% sobre o salário-base

namespace exercicio6 {
    let salario, salreceber, grat, imp: number;
    salario = 1000;
    grat = salario * 5/100;
    imp = salario * 7/100;
    salreceber = salario + grat - imp;

    console.log(`Salário final: ${salreceber}`);
    
}