//Faça um programa que calcule e mostre a área de um círculo. Sabe-se que: Área = piR^2

namespace exercicio10 {
    let area, raio: number;

    raio = 10;

    area = Math.PI * Math.pow(raio, 2);

    console.log(`A área do circulo: ${area}`);
    
}