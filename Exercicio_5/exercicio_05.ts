//Faça um programa que receba o salário de um funcionário e o percentual de aumento, calcule e mostre o valor do aumento e o novo salário 

namespace exercicio5 {
    let salario, percentual, aumento, novoSal: number;

    salario = 1000;
    percentual = 10;

    aumento = salario * percentual/100;

    console.log(`O aumento foi de ${aumento}`);

    novoSal = salario + aumento;

    console.log(`O novo salário do funcionário é de: ${novoSal}`);
    
    
}